import React from 'react';
import ReactDOM from 'react-dom';
import {Meteor} from 'meteor/meteor';
import {Tracker} from 'meteor/tracker';

import {Articles, calculateArticlePositions} from './../imports/api/articles';
import App from './../imports/ui/App';

Meteor.startup(() => {
  Tracker.autorun(() => {

    let positionedArticles = calculateArticlePositions(Articles);
    let title = 'Easy Trip';
    ReactDOM.render(<App title={title} articles={positionedArticles}/>, document.getElementById('app'));

  });
});
