import numeral from 'numeral';

export const Articles = [ 
  {_id: "AoL8FzCHHpWs3v28i", name: "asd", score: 0},
  {name: "sadasdsadasdasd", score: 0, _id: "tjxBP9ptLCqYf9n9m"}
];

export const calculateArticlePositions = (articles) => {
  let rank = 1;

  return articles.map((article, index) => {
    if (index !== 0 && articles[index - 1].score > article.score) {
      rank++;
    }

    return {
      ...article,
      rank,
      position: numeral(rank).format('0o')
    };
  });
};
